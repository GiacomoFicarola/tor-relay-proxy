FROM alpine:latest

# Installa Tor
RUN apk add --no-cache tor

# Copia il file di configurazione di base
COPY torrc.default /etc/tor/torrc.default

# Crea lo script di avvio in modo leggibile
RUN echo '#!/bin/sh' > /start.sh && \
    echo 'sed -i "s/NICKNAME_ENV/${NICKNAME}/" /etc/tor/torrc.default' >> /start.sh && \
    echo 'sed -i "s/MYFAMILY_ENV/${MYFAMILY}/" /etc/tor/torrc.default' >> /start.sh && \
    echo 'exec tor -f /etc/tor/torrc.default' >> /start.sh && \
    chmod +x /start.sh

# Imposta lo script di avvio come entrypoint
ENTRYPOINT ["/start.sh"]