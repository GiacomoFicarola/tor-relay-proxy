
# Tor Relay Proxy

This repository contains a Dockerized Tor relay proxy service, configured using a custom `torrc` file.

## Project Structure

- **Dockerfile**: Defines a lightweight Alpine-based Docker image with Tor installed and configured.
- **torrc.default**: Custom configuration file for Tor. It includes node restrictions and network policies.
- **.gitlab-ci.yml**: Pipeline configuration for continuous integration and deployment using GitLab CI/CD.

## Dockerfile Overview

The `Dockerfile` uses an Alpine image to install and configure Tor. The `torrc.default` file is copied into the image to define the Tor relay settings.

### Key Instructions:
- `apk update && apk add tor`: Installs Tor on Alpine.
- `COPY torrc.default /etc/tor/torrc.default`: Copies the custom Tor configuration into the container.
- `USER tor`: Runs the container as the `tor` user for security.
- `EXPOSE ${PORT}`: Exposes the port specified during the build.
- `ENTRYPOINT [ "tor" ]`: Sets the entrypoint to start the Tor service.
- `CMD [ "-f", "/etc/tor/torrc.default" ]`: Defines the configuration file to be used by Tor.

## Tor Configuration

The `torrc.default` file is customized to:
- Set up bandwidth and traffic accounting limits.
- Define SOCKS policies to accept traffic from specific subnets and reject all others.
- Restrict entry, middle, and exit nodes to specific countries.
- Ensure strict node policies with `StrictNodes 1`.

### Example Configuration:
```bash
Nickname AAAAAA
ExitRelay 0
SocksPort 0.0.0.0:9050
ContactInfo test@test.com
RelayBandwidthRate 400 KB
RelayBandwidthBurst 400 KB
AccountingStart day 00:00
AccountingMax 20 GB
SOCKSPolicy accept 192.168.1.0/24
SOCKSPolicy accept 172.16.0.0/12
SocksPolicy reject *
EntryNodes {AT},{BE},{BG},{CH},{CY},{CZ},{DE},{DK},{EE},{ES},{FI},{FR},{GB},{GR},{HU},{IE},{IT},{LT},{LU},{LV},{MT},{NL},{PL},{PT},{RO},{SE},{SI},{SK}
MiddleNodes {AT},{BE},{BG},{CH},{CY},{CZ},{DE},{DK},{EE},{ES},{FI},{FR},{GB},{GR},{HU},{IE},{IT},{LT},{LU},{LV},{MT},{NL},{PL},{PT},{RO},{SE},{SI},{SK}
ExitNodes {AT},{BE},{BG},{CH},{CY},{CZ},{DE},{DK},{EE},{ES},{FI},{FR},{GB},{GR},{HU},{IE},{IT},{LT},{LU},{LV},{MT},{NL},{PL},{PT},{RO},{SE},{SI},{SK}
StrictNodes 1
```

## GitLab CI/CD Pipeline

The GitLab CI/CD pipeline is set up to automatically build and release the Docker image on a scheduled basis.

### Key Steps:
- **Docker Build**: Builds the Docker image with the specified port argument.
- **Docker Login**: Logs into the GitLab container registry using CI environment variables.
- **Docker Push**: Pushes the built image to the GitLab container registry.

The pipeline only triggers on scheduled runs.

## Usage

1. Clone the repository.
2. Build the Docker image:
   ```bash
   docker build --build-arg PORT=9050 -t your-username/tor-relay-proxy .
   ```
3. Run the container:
   ```bash
   docker run -p 9050:9050 your-username/tor-relay-proxy
   ```

## License

This project is licensed under the GNU GENERAL PUBLIC LICENSE Version 3 - see the [LICENSE](./LICENSE) file for details.